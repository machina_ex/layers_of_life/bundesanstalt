# bundesarchiv

## Project setup
```
npm install
```

Um die Daten aus der Tabelle einzupflegen:

- Tabellen "Sheet2" und "Freigespielt" als CSV Datei exportieren 
- CSV Dateien unter https://csv.keyangxiang.com/#reviewResult in JSON konvertieren
- JSON text copy pasten und `/src/assets/erfassung.json` (Freigespielt) und `/src/assets/slides.json` (Sheet2) überschreiben.


#### PRINTING SERVICE

the printing service is addressed through HTTP GET REQUEST. The URL for the Printing Servers Endpoint is found at `SlideVier.vue` in line 78.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
